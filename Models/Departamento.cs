﻿using System.ComponentModel.DataAnnotations;

namespace Proyecto_sena.Models
{
    public class Departamento
    {
        [Key]
        public int DepartamentoID { get; set; }
        public string NombreDepartamento { get; set; }
    }
}
