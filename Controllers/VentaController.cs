﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Proyecto_sena.Data;
using Proyecto_sena.Models;
using System.Text.Json;
using System.Linq;
using Proyecto_sena.Models.DAOS;

namespace Proyecto_sena.Controllers
{
    public class VentaController : Controller
    {

        private readonly ILogger<VentaController> _logger;
        private readonly InnubeDbContext DbContext;

        public VentaController(ILogger<VentaController> logger, InnubeDbContext dbContext)
        {
            _logger = logger;
            this.DbContext = dbContext;
        }


        public IActionResult Home()
        {
            var value = HttpContext.Session.GetString("user");
            if (value == null)
            {
                return RedirectToAction("Login", "Login");
            }
            ClienteInnube cliente = JsonSerializer.Deserialize<ClienteInnube>(value);

            var query = from a in DbContext.Compra
                        join s in DbContext.Servicio on a.ServicioID equals s.ServicioID
                        join c in DbContext.ClienteInnube on a.ClienteInnubeID equals c.ClienteInnubeID
                        where s.ClienteInnubeID == cliente.ClienteInnubeID
                        select new MisVentas
                        {
                            ServicioID = s.ServicioID,
                            NombreServicio = s.Nombre,
                            Comprador = c.Nombre + " " + c.Apellido,
                            Cantidad = a.Cantidad,
                            ValorTotal = a.Valortotal,
                            FechaCompra = a.FechaInsercion

                        };

            return View(query.ToList());
        }
    }
}
