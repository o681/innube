﻿namespace Proyecto_sena.Models.DAOS
{
    public class MisCompras
    {
        public int CompraID { get; set; }
        public string NombreServicio { get; set; }
        public int Cantidad { get; set; }
        public decimal ValorUnidad { get; set; }
        public decimal ValorTotal { get; set; }
        public string IdCompra { get; set; }
        public string NombreEmpresa{ get; set; }
    }
}
