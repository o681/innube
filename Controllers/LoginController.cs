﻿using MailKit.Net.Smtp;
using MailKit.Security;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using MimeKit;
using Proyecto_sena.BLL;
using Proyecto_sena.Data;
using Proyecto_sena.Models;
using Proyecto_sena.Models.DAOS;
using Proyecto_sena.Settings;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace Proyecto_sena.Controllers
{
    public class LoginController : Controller
    {
        private readonly ILogger<LoginController> _logger;
        private readonly InnubeDbContext DbContext;
        private readonly MailSettings _mailSettings;

        public LoginController(ILogger<LoginController> logger, InnubeDbContext dbContext, IOptions<MailSettings> mailSettings)
        {
            _logger = logger;
            this.DbContext = dbContext;
            _mailSettings = mailSettings.Value;
        }


        public IActionResult Login()
        {
            return View();
        }

        public IActionResult Seleccion()
        {
            return View();
        }

        public IActionResult RegistroCliente()
        {
            return View();
        }

        public IActionResult RegistroEmpresa()
        {
            // Para conseguir la lista de ciudades
            List<SelectListItem> lista_ciudad_seleccion = new();

            var lista_ciudad = this.DbContext.Ciudad.ToList();

            foreach (var temp in lista_ciudad)
            {
                lista_ciudad_seleccion.Add(new SelectListItem { Value = temp.ciudadid.ToString(), Text = temp.nombreciudad });
            }

            // Para conseguir la lista de departamentos
            List<SelectListItem> lista_departamento_seleccion = new();

            var lista_departamento = this.DbContext.Departamento.ToList();

            foreach (var temp2 in lista_departamento)
            {
                lista_departamento_seleccion.Add(new SelectListItem { Value = temp2.DepartamentoID.ToString(), Text = temp2.NombreDepartamento });
            }

            ViewBag.lista_departamento = lista_departamento_seleccion;
            ViewBag.lista_ciudad = lista_ciudad_seleccion;
            return View();
        }

        public IActionResult Perfil()
        {
            var value = HttpContext.Session.GetString("user");
            if (value == null)
            {
                return RedirectToAction("Login", "Login");
            }
            ClienteInnube cliente = JsonSerializer.Deserialize<ClienteInnube>(value);
            ViewBag.Usuario = cliente;
            return View(cliente);
        }

        public IActionResult OlvidoContraseña()
        {
            return View();
        }

        [HttpPost]
        public IActionResult EnvioCorreo(IFormCollection formCollection)
        {
            string usuario = formCollection["correo"];
            var existe = DbContext.ClienteInnube.Where(x => x.CorreoElectronico == usuario).ToList();
            if (existe != null)
            {
                ClienteInnube cliente = existe[0];
                
                string salt = ContraseñaClienteDAO.RandomString(10);
                string nuevopassword = System.Guid.NewGuid().ToString();
                byte[] bytes_contraseña = Encoding.UTF8.GetBytes(nuevopassword);
                byte[] bytes_salt = Encoding.UTF8.GetBytes(salt);

                var parte_encriptada = ContraseñaClienteDAO.GenerateSaltedHash(bytes_contraseña, bytes_salt);

                cliente.ParteEncriptada = Convert.ToBase64String(parte_encriptada);
                cliente.Salt = salt;
                if (DbContext.SaveChanges() > 0)
                {

                    ViewBag.Mensaje = "Hemos enviado un correo electronico con su contraseña";
                    bool enviado = SendEmailAsync(new MailRequest()
                    {
                        Body = "Hemos cambiado tu contraseña, tu nueva contraseña es: " + nuevopassword,
                        Subject = "Cambio de contraseña",
                        ToEmail = cliente.CorreoElectronico
                    });
                    return View("Mensaje");
                }
                ViewBag.Mensaje = "Error, no podemos actualiazar la contraseña";
                return View("Mensaje");
            }
            else
            {
                ViewBag.Mensaje = "Lo sentimos, este usuario no esta registrado";
                return View("Mensaje");
            }
        }

        public bool SendEmailAsync(MailRequest mailRequest)
        {
            try
            {
                var email = new MimeMessage();
                email.Sender = MailboxAddress.Parse(_mailSettings.Mail);
                email.To.Add(MailboxAddress.Parse(mailRequest.ToEmail));
                email.Subject = mailRequest.Subject;
                var builder = new BodyBuilder();

                builder.HtmlBody = mailRequest.Body;
                email.Body = builder.ToMessageBody();
                using var smtp = new SmtpClient();
                smtp.Connect(_mailSettings.Host, _mailSettings.Port, SecureSocketOptions.StartTls);
                smtp.Authenticate(_mailSettings.Mail, _mailSettings.Password);
                smtp.Send(email);
                smtp.Disconnect(true);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        [HttpPost]
        public IActionResult ActualizarPerfil(IFormCollection formCollection)
        {
            string tipoPersona = formCollection["personanatural"];
            int id = Convert.ToInt16(formCollection["clienteid"]);
            ClienteInnube user = DbContext.ClienteInnube.Where(x => x.ClienteInnubeID == id).ToList()[0];
            if(string.IsNullOrEmpty(tipoPersona))
                user.Documento = formCollection["documento"];
            else
                user.Apellido = formCollection["apellido"];

            user.Nombre = formCollection["nombre"];            
            user.Telefono = formCollection["telefono"];
            user.Direccion = formCollection["direccion"];
            DbContext.SaveChanges();
            HttpContext.Session.SetString("user", JsonSerializer.Serialize(user));
            return RedirectToAction("Perfil");
        }

        public IActionResult RealizaLogin(IFormCollection formCollection)
        {
            string correo = formCollection["correo"];
            string password = formCollection["password"];
            var login = new ClienteInnubeBLL(this.DbContext).ExisteUsuario(correo, password);
            if (login != null)
            {
                HttpContext.Session.SetString("user", JsonSerializer.Serialize(login));
                return RedirectToAction("Home", "Plataforma");
            }
            ViewBag.Mensaje = "Error, el usuario no esta registrado en el sistema.";
            return View("Mensaje");
        }

        [HttpPost]
        public IActionResult RegistarCliente(IFormCollection formCollection)
        {
            string nombre = formCollection["nombre"];
            string apellido = formCollection["apellido"];
            string correo = formCollection["correo"];
            string correovalida = formCollection["confirmar_correo_cliente"];
            string contraseña = formCollection["password"];
            string validapassword = formCollection["confirmar_contraseña_cliente"];
            string documento = formCollection["documento"];

            if(validapassword != contraseña){
                ViewBag.Mensaje = "Error, las contraseñas no coinciden.";
                return View("Mensaje");
            }

            if (correo != correovalida)
            {
                ViewBag.Mensaje = "Error, los correos no coinciden.";
                return View("Mensaje");
            }


            ClienteInnube clie = new ClienteInnube();
            clie.Nombre = nombre;
            clie.Apellido = apellido;
            clie.CorreoElectronico = correo;
            clie.Documento = documento;

            string salt = ContraseñaClienteDAO.RandomString(10);
            byte[] bytes_contraseña = Encoding.UTF8.GetBytes(contraseña);
            byte[] bytes_salt = Encoding.UTF8.GetBytes(salt);

            var parte_encriptada = ContraseñaClienteDAO.GenerateSaltedHash(bytes_contraseña, bytes_salt);

            clie.ParteEncriptada = Convert.ToBase64String(parte_encriptada);
            clie.Salt = salt;
            clie.PersonaNatural = true;
            clie.PersonaJuridica = false;

            //Antes de guardar valida si el correo existe
            var existe = DbContext.ClienteInnube.Where(s => s.CorreoElectronico.ToUpper() == clie.CorreoElectronico.ToUpper())
                                                  .ToList();

            if (existe != null && existe.Count > 0)
            {
                ViewBag.Mensaje = "Error, este cliente ya esta registrado, por favor ingrese con su correo y la contraseña";
                return View("Mensaje");
            }

            this.DbContext.ClienteInnube.Add(clie);

            int vRet = this.DbContext.SaveChanges();
            if (vRet > 0)
            {
                HttpContext.Session.SetString("user", JsonSerializer.Serialize(clie));
                var value = HttpContext.Session.GetString("user");
                ClienteInnube cliente = JsonSerializer.Deserialize<ClienteInnube>(value);
                return RedirectToAction("Home", "Plataforma");
            }

            ViewBag.Mensaje = "Error, no es posible registrar el cliente";
            return View("Mensaje");
        }


        [HttpPost]
        public IActionResult RegistrarEmpresa(IFormCollection formCollection)
        {
            string nombre = formCollection["nombre"];
            string documento = formCollection["documento"];
            string telefono = formCollection["telefono"];
            string correo = formCollection["correo_electronico_empresa"];
            string correovalida = formCollection["confirme_correo"];
            string direccion = formCollection["DireccionCompañia"];
            string id_ciudad = formCollection["IdCiudad"];
            string id_departamento = formCollection["IdDepartamento"];
            string contraseña = formCollection["password"];
            string contraseñavalida = formCollection["contraseñaempresa"];

            if (contraseñavalida != contraseña)
            {
                ViewBag.Mensaje = "Error, las contraseñas no coinciden.";
                return View("Mensaje");
            }

            if (correo != correovalida)
            {
                ViewBag.Mensaje = "Error, los correos no coinciden.";
                return View("Mensaje");
            }

            ClienteInnube clie = new ClienteInnube();
            clie.Nombre = nombre;
            clie.Telefono = telefono;
            clie.CorreoElectronico = correo;
            clie.Direccion = direccion;
            clie.Documento = documento;
            clie.CiudadID = UInt16.Parse(id_ciudad);
            clie.DepartamentoID = UInt16.Parse(id_departamento);

            string salt = ContraseñaClienteCompañiumDAO.RandomString(10);
            byte[] bytes_contraseña = Encoding.UTF8.GetBytes(contraseña);
            byte[] bytes_salt = Encoding.UTF8.GetBytes(salt);

            var parte_encriptada = ContraseñaClienteCompañiumDAO.GenerateSaltedHash(bytes_contraseña, bytes_salt);

            clie.ParteEncriptada = Convert.ToBase64String(parte_encriptada);
            clie.Salt = salt;
            clie.PersonaNatural = false;
            clie.PersonaJuridica = true;

            //Antes de guardar valida si el correo existe
            var existe = DbContext.ClienteInnube.Where(s => s.CorreoElectronico.ToUpper() == clie.CorreoElectronico.ToUpper())
                                                  .ToList();

            if (existe != null && existe.Count > 0)
            {
                ViewBag.Mensaje = "Error, este cliente ya esta registrado, por favor ingrese con su correo y la contraseña";
                return View("Mensaje");
            }

            this.DbContext.ClienteInnube.Add(clie);

            int vRet = this.DbContext.SaveChanges();
            if (vRet > 0)
            {
                HttpContext.Session.SetString("user", JsonSerializer.Serialize(clie));
                var value = HttpContext.Session.GetString("user");
                ClienteInnube cliente = JsonSerializer.Deserialize<ClienteInnube>(value);
                return RedirectToAction("Home", "Plataforma");
            }

            ViewBag.Mensaje = "Error, no es posible registrar esta empresa";
            return View("Mensaje");
        }

    }
}
