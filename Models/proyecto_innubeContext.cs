﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace Proyecto_sena.Models
{
    public partial class proyecto_innubeContext : DbContext
    {
      
        public proyecto_innubeContext(DbContextOptions<proyecto_innubeContext> options) : base(options)
        {

        }

        public virtual DbSet<CiudadCompañium> CiudadCompañia { get; set; }
        public virtual DbSet<Cliente> Clientes { get; set; }
        public virtual DbSet<ClienteCompañium> ClienteCompañia { get; set; }
        public virtual DbSet<ClienteGeneral> ClienteGenerals { get; set; }
        public virtual DbSet<Compañium> Compañia { get; set; }
        public virtual DbSet<ContraseñaCliente> ContraseñaClientes { get; set; }
        public virtual DbSet<ContraseñaClienteCompañium> ContraseñaClienteCompañia { get; set; }
        public virtual DbSet<ContraseñaCompañium> ContraseñaCompañia { get; set; }
        public virtual DbSet<Cotizacion> Cotizacions { get; set; }
        public virtual DbSet<DemandaServicio> DemandaServicios { get; set; }
        public virtual DbSet<DepartamentoCompañium> DepartamentoCompañia { get; set; }
        public virtual DbSet<Evaluacion> Evaluacions { get; set; }
        public virtual DbSet<OfertaServicio> OfertaServicios { get; set; }
        public virtual DbSet<Parametro> Parametros { get; set; }
        public virtual DbSet<ResultadoEvaluacion> ResultadoEvaluacions { get; set; }
        public virtual DbSet<ServicioOfrecido> ServicioOfrecidos { get; set; }

    }
}
