﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

#nullable disable

namespace Proyecto_sena.Models
{
    public partial class DemandaServicio
    {
        [Key]
        public uint IdCotizacion { get; set; }
        public string IdServicio { get; set; }
        public DateTime? FechaCotizacion { get; set; }
        public sbyte? Cantidad { get; set; }

        public virtual Cotizacion IdCotizacionNavigation { get; set; }
        public virtual ServicioOfrecido IdServicioNavigation { get; set; }
    }
}
