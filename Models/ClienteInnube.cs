﻿using System.ComponentModel.DataAnnotations;

namespace Proyecto_sena.Models
{
    public class ClienteInnube
    {
        [Key]
        public int ClienteInnubeID { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string CorreoElectronico { get; set; }
        public string Telefono { get; set; }
        public string Direccion { get; set; }
        public string Documento { get; set; }
        public int CiudadID { get; set; }
        public int DepartamentoID { get; set; }
        public string Salt { get; set; }
        public string ParteEncriptada { get; set; }
        public bool PersonaNatural { get; set; }
        public bool PersonaJuridica { get; set; }
    }
}
