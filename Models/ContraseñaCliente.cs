﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

#nullable disable

namespace Proyecto_sena.Models
{
    [Table("contraseña_cliente")]
    public class ContraseñaCliente
    {
        public ContraseñaCliente()
        {
            Clientes = new HashSet<Cliente>();
        }

        [Key]
        [Column("id_contraseña_cliente")]
        public uint IdContraseñaCliente { get; set; }
        [Column("salt")]
        public string Salt { get; set; }
        [Column("parte_encriptada")]
        public string ParteEncriptada { get; set; }

        public ContraseñaCliente(string salt, string parteEncriptada)
        {
            Salt = salt;
            ParteEncriptada = parteEncriptada;
        }

        public virtual ICollection<Cliente> Clientes { get; set; }

    }
}
