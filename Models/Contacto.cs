﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Proyecto_sena.Models
{
    public class Contacto
    {
        [Key]
        public int Contactoid {get;set;}
        public string Nombre { get;set;}
        public string Apellido { get; set; }
        public string Correoelectronico { get; set; }
        public string Telefono { get; set; }
        public DateTime Fechainsercion{ get; set; }

    }
}
