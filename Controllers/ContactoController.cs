﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Proyecto_sena.Data;

namespace Proyecto_sena.Controllers
{
    public class ContactoController : Controller
    {
        private readonly ILogger<ContactoController> _logger;
        private readonly InnubeDbContext DbContext;

        public ContactoController(ILogger<ContactoController> logger, InnubeDbContext dbContext)
        {
            _logger = logger;
            this.DbContext = dbContext;
        }

        public IActionResult Home()
        {
            return View();
        }


        public IActionResult RegistrarContacto(IFormCollection formCollection)
        {
            this.DbContext.Contacto.Add(new Models.Contacto()
            {
                Apellido = formCollection["apellido"],
                Nombre = formCollection["nombre"],
                Correoelectronico = formCollection["correo"],
                Telefono = formCollection["telefono"],
                Fechainsercion = System.DateTime.Now
            });

            this.DbContext.SaveChanges();
            ViewBag.Mensaje = "Hemos recibido tu información en breve estaremos en contacto contigo";
            return View("Home");
        }

        public IActionResult RegistrarContactoFromInicio(IFormCollection formCollection)
        {
            this.DbContext.Contacto.Add(new Models.Contacto()
            {
                Apellido = formCollection["apellido"],
                Nombre = formCollection["nombre"],
                Correoelectronico = formCollection["correo"],
                Telefono = formCollection["telefono"],
                Fechainsercion = System.DateTime.Now
            });

            this.DbContext.SaveChanges();
            ViewBag.Mensaje = "Hemos recibido tu información en breve estaremos en contacto contigo";
            return RedirectToAction("Inicio","Home");
        }
    }
}
