﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Proyecto_sena.Data;
using Proyecto_sena.Models;
using Proyecto_sena.Models.DAOS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;

namespace Proyecto_sena.Controllers
{
    public class CompraController : Controller
    {
        private readonly ILogger<CompraController> _logger;
        private readonly InnubeDbContext DbContext;

        public CompraController(ILogger<CompraController> logger, InnubeDbContext dbContext)
        {
            _logger = logger;
            this.DbContext = dbContext;
        }

        public IActionResult Home()
        {
            var servicios = DbContext.Servicio.ToList();
            return View(servicios);
        }

        [HttpPost]
        public IActionResult AdicionarCarrito(IFormCollection formCollection)
        {
            var value = HttpContext.Session.GetString("carrito");
            if (value == null)
            {
                List<Carrito> carritoCompras = new List<Carrito>();
                HttpContext.Session.SetString("carrito", JsonSerializer.Serialize(carritoCompras));
            }
            value = HttpContext.Session.GetString("carrito");
            List<Carrito> carrito = JsonSerializer.Deserialize<List<Carrito>>(value);

            Carrito nuevoitem = new Carrito();
            int idservicio = Convert.ToInt16(formCollection["servicioid"]);
            var infoservicio = DbContext.Servicio.Where(s => s.ServicioID == idservicio).ToList();
            nuevoitem.Nombre = infoservicio[0].Nombre;
            nuevoitem.Valor = infoservicio[0].Valor;
            nuevoitem.ServicioID = idservicio;
            nuevoitem.ItemID = Guid.NewGuid().ToString();
            nuevoitem.Cantidad = Convert.ToInt16(formCollection["cantidad"]);
            carrito.Add(nuevoitem);
            HttpContext.Session.SetString("carrito", JsonSerializer.Serialize(carrito));
            return View("Carrito", carrito);
        }

        public IActionResult Carrito()
        {
            var value = HttpContext.Session.GetString("carrito");
            if (value == null)
            {
                return View("Carrito", new List<Carrito>());
            }
            List<Carrito> carrito = JsonSerializer.Deserialize<List<Carrito>>(value);
            return View("Carrito", carrito);
        }

        [HttpPost]
        public IActionResult RealizarCompra()
        {
            var value = HttpContext.Session.GetString("carrito");
            if (value == null)
            {
                ViewBag.Mensaje = "Error, no tienes nada para comprar en tu carrito";
                return View("Mensaje");
            }
            List<Carrito> carrito = JsonSerializer.Deserialize<List<Carrito>>(value);
            if (carrito == null || carrito.Count <= 0)
            {
                ViewBag.Mensaje = "Error, no tienes nada para comprar en tu carrito";
                return View("Mensaje");
            }
            var clie = HttpContext.Session.GetString("user");
            ClienteInnube cliente = JsonSerializer.Deserialize<ClienteInnube>(clie);
            if (cliente == null)
            {
                ViewBag.Mensaje = "Lo sentimos, su sesión expiro, por favor inicie de nuevo.";
                return View("Mensaje");
            }


            string uuid = Guid.NewGuid().ToString();
            foreach (Carrito item in carrito)
            {
                Compra newCompra = new Compra();
                newCompra.Valorunidad = item.Valor;
                newCompra.Cantidad = item.Cantidad;
                newCompra.IdCompra = uuid;
                newCompra.Valortotal = item.Cantidad * item.Valor;
                newCompra.ClienteInnubeID = cliente.ClienteInnubeID;
                newCompra.ServicioID = item.ServicioID;
                newCompra.FechaInsercion = DateTime.Now;
                this.DbContext.Compra.Add(newCompra);
                this.DbContext.SaveChanges();
            }

            ViewBag.Mensaje = "Felicitaciones compra exitosa";
            return View("Mensaje");
        }

        public IActionResult MisCompras()
        {
            var clie = HttpContext.Session.GetString("user");
            ClienteInnube cliente = JsonSerializer.Deserialize<ClienteInnube>(clie);
            if (cliente == null)
            {
                ViewBag.Mensaje = "Lo sentimos, su sesión expiro, por favor inicie de nuevo.";
                return View("Mensaje");
            }

            var query = from a in DbContext.Compra
                        join s in DbContext.Servicio on a.ServicioID equals s.ServicioID
                        join c in DbContext.ClienteInnube on s.ClienteInnubeID equals c.ClienteInnubeID
                        where a.ClienteInnubeID == cliente.ClienteInnubeID
                        select new MisCompras
                        {
                            CompraID = a.CompraID,
                            NombreServicio = s.Nombre,
                            Cantidad = a.Cantidad,
                            ValorUnidad = a.Valorunidad,
                            ValorTotal = a.Valortotal,
                            IdCompra = a.IdCompra,
                            NombreEmpresa = c.Nombre
                        };

            return View(query.ToList());
        }

        public IActionResult EliminarCarrito(string id)
        {
            var value = HttpContext.Session.GetString("carrito");
            if (value == null)
            {
                return View("Carrito", new List<Carrito>());
            }
            List<Carrito> carrito = JsonSerializer.Deserialize<List<Carrito>>(value);

            if(carrito.Count > 0){
                carrito.RemoveAll(x => x.ItemID == id);
            }
            HttpContext.Session.SetString("carrito", JsonSerializer.Serialize(carrito));
            return View("Carrito", carrito);
        }
    }
}

