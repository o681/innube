﻿using System;

namespace Proyecto_sena.Models.DAOS
{
    public class MisVentas
    {
        public int ServicioID{ get; set; }
        public string NombreServicio{ get; set; }
        public string Comprador { get; set; }
        public int Cantidad{ get; set; }
        public decimal ValorTotal{ get; set; }
        public DateTime FechaCompra{ get; set; }
    }
}
