﻿using Microsoft.EntityFrameworkCore;
using Proyecto_sena.Models;

namespace Proyecto_sena.Data
{
    public class InnubeDbContext : DbContext
    {
        public InnubeDbContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<Ciudad> Ciudad{ get; set; }
        public DbSet<Departamento> Departamento { get; set; }
        public DbSet<ClienteInnube> ClienteInnube { get; set; }
        public DbSet<Servicio> Servicio { get; set; }
        public DbSet<Compra> Compra { get; set; }
        public DbSet<Contacto> Contacto{ get; set; }
    }
}
