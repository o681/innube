﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

#nullable disable

namespace Proyecto_sena.Models
{
    public partial class ResultadoEvaluacion
    {
        [Key]
        public string IdCompañia { get; set; }
        public uint IdEvaluacion { get; set; }
        public bool Aprobado { get; set; }
        public DateTime FechaAprobado { get; set; }
        public sbyte Calificacion { get; set; }

        public virtual Compañium IdCompañiaNavigation { get; set; }
        public virtual Evaluacion IdEvaluacionNavigation { get; set; }
    }
}
