﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

#nullable disable

namespace Proyecto_sena.Models
{
    public partial class ContraseñaCompañium
    {
        public ContraseñaCompañium()
        {
            Compañia = new HashSet<Compañium>();
        }
        [Key]
        public uint IdContraseñaCompañia { get; set; }
        public string Salt { get; set; }
        public string ParteEncriptada { get; set; }

        public virtual ICollection<Compañium> Compañia { get; set; }
    }
}
