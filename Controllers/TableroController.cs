using System;
using System.Collections.Generic;
using System.Diagnostics;

using System.Linq;
using System.Text.Json;
// using System.Text.Json.Serialization;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Proyecto_sena.Models;

namespace Proyecto_sena.Controllers
{
    public class TableroController : Controller
    {
        private readonly proyecto_innubeContext _context;
        private readonly ILogger<TableroController> _logger;

        public TableroController(ILogger<TableroController> logger, proyecto_innubeContext context)
        {
            _logger = logger;
            _context = context;
        }

        [Authorize]
        public IActionResult Index()
        {
            return View();
        }

        [Authorize]
        public IActionResult Buscar()
        {
            List<String> lista_datos = new List<string>();

            var oferta_servicios = this._context.OfertaServicios.ToList();
            foreach (var ofer in oferta_servicios)
            {
                var servicio = this._context.ServicioOfrecidos.FirstOrDefault(u => u.IdServicio == ofer.IdServicio);
                lista_datos.Add(servicio.IdServicio);
                lista_datos.Add(servicio.NombreServicio);
                lista_datos.Add(servicio.PrecioServicio.ToString());
                var comp = this._context.Compañia.FirstOrDefault(u => u.IdCompañia == ofer.IdCompañia);
                lista_datos.Add(comp.NombreCompañia);

            }

            ViewBag.longitud = lista_datos.Count();
            ViewBag.lista = lista_datos;
            return View();
        }

        [Authorize]
        public IActionResult MostrarDatos()
        {
            var correo = User.Identity.Name;
            var persona = this._context.Clientes.FirstOrDefault(u => u.CorreoElectronicoCliente == correo);

            ViewBag.persona = persona;
            return View();
        }

        [Authorize]
        public IActionResult Editar()
        {
            var correo = User.Identity.Name;
            var persona = this._context.Clientes.FirstOrDefault(u => u.CorreoElectronicoCliente == correo);

            ViewBag.persona = persona;
            return View();
        }

        [Authorize]
        public IActionResult EditarCliente(IFormCollection formCollection)
        {
            string nombre = formCollection["nombre"];
            string apellido = formCollection["apellido"];
            string correo = formCollection["correo"];

            var correo_original = User.Identity.Name;
            var persona = this._context.Clientes.FirstOrDefault(u => u.CorreoElectronicoCliente == correo_original);

            if (!nombre.Equals(""))
            {
                persona.NombreCliente = nombre;
            }

            if (!apellido.Equals(""))
            {
                persona.ApellidoCliente = apellido;
            }

            if (!correo.Equals(""))
            {
                persona.CorreoElectronicoCliente = correo;
            }

            this._context.SaveChanges();

            return RedirectToAction("Index");
        }

        [Authorize]
        public IActionResult CogerDatos([FromBody] JsonDocument values)
        {
            List<string> listas_ids = new List<string>();

            Console.WriteLine("Probando");
            JsonElement a = values.RootElement;
            JsonElement ids = a.GetProperty("ids");
            foreach (var c in ids.EnumerateArray())
            {
                if (c.TryGetProperty("id", out JsonElement valor_id))
                {
                    listas_ids.Add(valor_id.GetString());
                }
            }
            TempData["lista_ids"] = listas_ids;

            return RedirectToAction("MostrarServicios", new { ll = listas_ids });
        }

        [Authorize]
        [HttpGet]
        // public IActionResult MostrarServicios([FromBody] JsonDocument values)
        public IActionResult MostrarServicios(List<string> ll)
        {
            List<String> lista_datos = new List<string>();
            // var ids = CogerDatos(values);
            var ids = ll;

            IEnumerable<string> ids2 = ids as IEnumerable<string>;

            foreach (var item in ids)
            {
                var servicio = this._context.ServicioOfrecidos.FirstOrDefault(u => u.IdServicio.Equals(item));
                lista_datos.Add(servicio.IdServicio);
                lista_datos.Add(servicio.NombreServicio);
                lista_datos.Add(servicio.PrecioServicio.ToString());
            }

            ViewBag.longitud = lista_datos.Count();
            ViewBag.lista = lista_datos;

            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
