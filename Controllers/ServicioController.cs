﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Proyecto_sena.Data;
using Proyecto_sena.Models;
using System;
using System.Linq;
using System.Text.Json;

namespace Proyecto_sena.Controllers
{
    public class ServicioController : Controller
    {

        private readonly ILogger<ServicioController> _logger;
        private readonly InnubeDbContext DbContext;

        public ServicioController(ILogger<ServicioController> logger, InnubeDbContext dbContext)
        {
            _logger = logger;
            this.DbContext = dbContext;
        }

        public IActionResult MisServiciosOfertados()
        {
            var value = HttpContext.Session.GetString("user");
            ClienteInnube cliente = JsonSerializer.Deserialize<ClienteInnube>(value);
            ViewBag.Usuario = cliente;
            //Antes de guardar valida si el correo existe
            var misServicios = DbContext.Servicio.Where(s => s.ClienteInnubeID == cliente.ClienteInnubeID).ToList();
            return View(misServicios);
        }

        public IActionResult AddServicio(){
            return View();
        }

        [HttpPost]
        public IActionResult RegistrarServicio(IFormCollection formCollection)
        {
            var value = HttpContext.Session.GetString("user");
            ClienteInnube cliente = JsonSerializer.Deserialize<ClienteInnube>(value);
            if(cliente == null){
                ViewBag.Mensaje = "Lo sentimos, su sesión expiro, por favor inicie de nuevo.";
                return View("Mensaje");
            }

            Servicio nuevo = new Servicio();
            nuevo.Nombre = formCollection["nombre"];
            nuevo.Descripcion = formCollection["descripcion"];
            nuevo.Valor =Convert.ToDecimal(formCollection["valor"]);
            nuevo.ClienteInnubeID = cliente.ClienteInnubeID;

            this.DbContext.Servicio.Add(nuevo);

            int vRet = this.DbContext.SaveChanges();
            if (vRet > 0)
            {
                ViewBag.Mensaje = "Servicio registrado exitosamente.";
                return View("Mensaje");
            }

            ViewBag.Mensaje = "Error, no es posible registrar el servicio";
            return View("Mensaje");
        }

    }
}
