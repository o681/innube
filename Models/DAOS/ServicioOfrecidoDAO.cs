using System;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Proyecto_sena.Models;
using System.Collections.Generic;
using System.Linq;

#nullable disable

namespace Proyecto_sena.Models.DAOS
{
    public class ServicioOfrecidoDAO
    {

        private readonly proyecto_innubeContext _context;
        public ServicioOfrecidoDAO(proyecto_innubeContext context)
        {
            _context = context;
        }

        public static string CrearId()
        {
            var id_nuevo = new StringBuilder();
            string inicio_exp = "SO";
            Random num_random = new Random();
            var num = num_random.Next(10000000, 99999999);
            id_nuevo.Append(inicio_exp);
            id_nuevo.Append(num);
            return id_nuevo.ToString();
        }

        public ContraseñaCliente ExisteUsuario(string correo, string contraseña)
        {
            var existe = this._context.Clientes.ToList().Exists(c => c.CorreoElectronicoCliente == correo);

            if (!existe) {
                return null;
            }

            Cliente persona = this._context.Clientes.FirstOrDefault(c => c.CorreoElectronicoCliente == correo);

            uint? id_contraseña = persona.IdContraseñaCliente;

            ContraseñaCliente contra = this._context.ContraseñaClientes.Find(id_contraseña);

            var salt = contra.Salt;
            byte[] bytes_contraseña = Encoding.UTF8.GetBytes(contraseña);
            byte[] bytes_salt = Encoding.UTF8.GetBytes(salt);

            var contraseña_encriptada = ContraseñaClienteDAO.GenerateSaltedHash(bytes_contraseña, bytes_salt);
            var contraseña_encriptada2 = Convert.ToBase64String(contraseña_encriptada);

            existe = this._context.ContraseñaClientes.ToList().Exists(u => u.ParteEncriptada == contraseña_encriptada2);

            if (!existe) {
                return null;
            }

            var usuarioLogueado = this._context.ContraseñaClientes.FirstOrDefault(u => u.ParteEncriptada == contraseña_encriptada2);

            return usuarioLogueado;
        }
    }
}
