﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Proyecto_sena.Models;
using System.Text.Json;

namespace Proyecto_sena.Controllers
{
    public class PlataformaController : Controller
    {
        public IActionResult Home()
        {
            var value = HttpContext.Session.GetString("user");
            if(value == null){
                return RedirectToAction("Login","Login");
            }
            ClienteInnube cliente = JsonSerializer.Deserialize<ClienteInnube>(value);
            ViewBag.Usuario = cliente;
            return View();
        }
    }
}
