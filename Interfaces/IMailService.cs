﻿using Proyecto_sena.Models;
using System.Threading.Tasks;

namespace Proyecto_sena.Interfaces
{
    public interface IMailService
    {
        Task SendEmailAsync(MailRequest mailRequest);
    }
}
