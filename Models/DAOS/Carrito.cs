﻿namespace Proyecto_sena.Models.DAOS
{
    public class Carrito
    {
        public string ItemID{ get; set; }
        public int ServicioID { get; set; }
        public string Nombre { get; set; }
        public decimal Valor { get; set; }
        public string NombreEmpresa { get; set; }
        public int Cantidad{ get; set; }
    }
}
