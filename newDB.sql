/*
create database innube;
use innube;

create table clienteinnube
(
	clienteinnubeid int not null auto_increment primary key,
    nombre varchar(100),
    apellido varchar(100),
    correoelectronico varchar(150),
    telefono varchar(80),
    direccion varchar(150),
    documento varchar(100),
    ciudadid int,
    departamentoid int,
    salt varchar(20),
    parteencriptada varchar(150),
    personanatural bit,
    personajuridica bit
);

create table departamento
(
	departamentoid int not null auto_increment primary key,
    nombredepartamento varchar(100)
);

insert into departamento(nombredepartamento)
values('Antioquia');
insert into departamento(nombredepartamento)
values('Valle del Cauca');
insert into departamento(nombredepartamento)
values('Santander');
insert into departamento(nombredepartamento)
values('Barranquilla');


create table ciudad
(
	ciudadid int not null auto_increment primary key,
    nombreciudad varchar(100)
);

insert into ciudad(nombreciudad)
values('Bogotá');
insert into ciudad(nombreciudad)
values('Medellin');

create table servicio
(
    servicioid int not null auto_increment primary key,
    clienteinnubeid int,
    nombre varchar(100),
    valor decimal(12,2),
    descripcion varchar(1000)
);


create table compra
(
    compraid int not null auto_increment primary key,
    clienteinnubeid int,
    servicioid int, 
    cantidad int,
    valorunidad decimal(12,2),
    valortotal decimal(12,2),
    idcompra varchar(200),
    fechainsercion datetime not null default(now())
);


create table contacto
(
    contactoid int not null auto_increment primary key,
	nombre varchar(100),
    apellido varchar(100),
    correoelectronico varchar(150),
    telefono varchar(80),
	fechainsercion datetime not null default(now())
);

CREATE USER 'innube'@'localhost' IDENTIFIED BY 'Temporal123';
GRANT ALL PRIVILEGES ON * . * TO 'innube'@'localhost';
FLUSH PRIVILEGES;

*/