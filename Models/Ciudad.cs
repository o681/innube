﻿using System.ComponentModel.DataAnnotations;

namespace Proyecto_sena.Models
{
    public class Ciudad
    {
        [Key]
        public int ciudadid { get; set; }
        public string nombreciudad { get; set; }
    }
}
