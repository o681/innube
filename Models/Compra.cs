﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Proyecto_sena.Models
{
    public class Compra
    {
        [Key]
        public int CompraID { get; set; }
        public int ClienteInnubeID { get; set; }
        public int ServicioID { get; set; }
        public int Cantidad { get; set; }
        public decimal Valorunidad { get; set; }
        public decimal Valortotal { get; set; }
        public string IdCompra { get; set; }
        public DateTime FechaInsercion { get; set; }
    }
}
