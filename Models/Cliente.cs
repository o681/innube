﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

#nullable disable

namespace Proyecto_sena.Models
{
    [Table("cliente")]
    public class Cliente
    {
        [Key]
        [Column("id_cliente")]
        public string IdCliente { get; set; }
        [Column("nombre_cliente")]
        public string NombreCliente { get; set; }
        [Column("apellido_cliente")]
        public string ApellidoCliente { get; set; }

        [EmailAddress]
        [Column("correo_electronico_cliente")]
        public string CorreoElectronicoCliente { get; set; }

        [Column("id_contraseña_cliente")]
        public uint IdContraseñaCliente { get; set; }

      
    }
}
