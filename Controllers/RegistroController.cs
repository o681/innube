using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using Proyecto_sena.Data;
using Proyecto_sena.Models;
using Proyecto_sena.Models.DAOS;

namespace Proyecto_sena.Controllers
{
    public class RegistroController : Controller
    {
        private readonly ILogger<RegistroController> _logger;
        private readonly proyecto_innubeContext _context;
        private readonly InnubeDbContext DbContext;

        public RegistroController(ILogger<RegistroController> logger, proyecto_innubeContext context,InnubeDbContext dbContext)
        {
            _logger = logger;
            this._context = context;
            this.DbContext = dbContext;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult RegCliente()
        {
            return View();
        }

        public IActionResult RegEmpresa()
        {

            // Para conseguir la lista de ciudades
            List<SelectListItem> lista_ciudad_seleccion = new();

            var lista_ciudad = this.DbContext.Ciudad.ToList();

            foreach (var temp in lista_ciudad)
            {
                lista_ciudad_seleccion.Add(new SelectListItem { Value = temp.ciudadid.ToString(), Text = temp.nombreciudad });
            }

            // Para conseguir la lista de departamentos
            List<SelectListItem> lista_departamento_seleccion = new();

            var lista_departamento = this.DbContext.Departamento.ToList();

            foreach (var temp2 in lista_departamento)
            {
                lista_departamento_seleccion.Add(new SelectListItem { Value = temp2.DepartamentoID.ToString(), Text = temp2.NombreDepartamento });
            }

            ViewBag.lista_departamento = lista_departamento_seleccion;
            ViewBag.lista_ciudad = lista_ciudad_seleccion;

            return View();
        }

        public IActionResult RegEmpresaOfertante()
        {


            // Para conseguir la lista de ciudades
            List<SelectListItem> lista_ciudad_seleccion = new();

            var lista_ciudad = this.DbContext.Ciudad.ToList();

            foreach (var temp in lista_ciudad)
            {
                lista_ciudad_seleccion.Add(new SelectListItem { Value = temp.ciudadid.ToString(), Text = temp.nombreciudad });
            }

            // Para conseguir la lista de departamentos
            List<SelectListItem> lista_departamento_seleccion = new();

            var lista_departamento = this.DbContext.Departamento.ToList();

            foreach (var temp2 in lista_departamento)
            {
                lista_departamento_seleccion.Add(new SelectListItem { Value = temp2.DepartamentoID.ToString(), Text = temp2.NombreDepartamento });
            }

            ViewBag.lista_departamento = lista_departamento_seleccion;
            ViewBag.lista_ciudad = lista_ciudad_seleccion;

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult CrearCliente(IFormCollection formCollection)
        {
            string nombre = formCollection["nombre"];
            string apellido = formCollection["apellido"];
            string correo = formCollection["correo"];
            string contraseña = formCollection["contraseña"];

            ClienteInnube clie = new ClienteInnube();
            clie.Nombre = nombre;
            clie.Apellido = apellido;
            clie.CorreoElectronico = correo;

            string salt = ContraseñaClienteDAO.RandomString(10);
            byte[] bytes_contraseña = Encoding.UTF8.GetBytes(contraseña);
            byte[] bytes_salt = Encoding.UTF8.GetBytes(salt);

            var parte_encriptada = ContraseñaClienteDAO.GenerateSaltedHash(bytes_contraseña, bytes_salt);

            clie.ParteEncriptada = Convert.ToBase64String(parte_encriptada);
            clie.Salt = salt;
            clie.PersonaNatural = true;
            clie.PersonaJuridica = false;

            this.DbContext.ClienteInnube.Add(clie);

            this.DbContext.SaveChanges();

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult CrearCompañiaCliente(IFormCollection formCollection)
        {

            string nombre = formCollection["NombreCompañia"];
            string telefono = formCollection["TelefonoCompañia"];
            string correo = formCollection["CorreoElectronicoCompañia"];
            string direccion = formCollection["DireccionCompañia"];
            string nit_compañia = formCollection["NitCompañia"];
            string id_ciudad = formCollection["IdCiudad"];
            string id_departamento = formCollection["IdDepartamento"];
            string contraseña = formCollection["contraseña"];

            ClienteInnube clie = new ClienteInnube();
            clie.Nombre = nombre;
            clie.Telefono = telefono;
            clie.CorreoElectronico = correo;
            clie.Direccion = direccion;
            clie.Documento = nit_compañia;
            clie.CiudadID = UInt16.Parse(id_ciudad);
            clie.DepartamentoID = UInt16.Parse(id_departamento);

            string salt = ContraseñaClienteCompañiumDAO.RandomString(10);
            byte[] bytes_contraseña = Encoding.UTF8.GetBytes(contraseña);
            byte[] bytes_salt = Encoding.UTF8.GetBytes(salt);

            var parte_encriptada = ContraseñaClienteCompañiumDAO.GenerateSaltedHash(bytes_contraseña, bytes_salt);

            clie.ParteEncriptada = Convert.ToBase64String(parte_encriptada);
            clie.Salt = salt;
            clie.PersonaNatural = true;
            clie.PersonaJuridica = false;        
      
         
            this.DbContext.SaveChanges();

            return View("CrearCliente");
        }

        public IActionResult CrearCompañiaOfertante(IFormCollection formCollection)
        {

            string nombre = formCollection["NombreCompañia"];
            string telefono = formCollection["TelefonoCompañia"];
            string correo = formCollection["CorreoElectronicoCompañia"];
            string direccion = formCollection["DireccionCompañia"];
            string nit_compañia = formCollection["NitCompañia"];
            string id_ciudad = formCollection["IdCiudad"];
            string id_departamento = formCollection["IdDepartamento"];
            string contraseña = formCollection["contraseña"];

            Compañium clie = new Compañium();
            clie.IdCompañia = new CompañiumDAO(this._context).CrearId();
            clie.NombreCompañia = nombre;
            clie.TelefonoCompañia = telefono;
            clie.CorreoElectronicoCompañia = correo;
            clie.DireccionCompañia = direccion;
            clie.NitCompañia = nit_compañia;
            clie.IdCiudad = UInt16.Parse(id_ciudad);
            clie.IdDepartamento = UInt16.Parse(id_departamento);

            string salt = ContraseñaCompañiumDAO.RandomString(10);
            byte[] bytes_contraseña = Encoding.UTF8.GetBytes(contraseña);
            byte[] bytes_salt = Encoding.UTF8.GetBytes(salt);

            var parte_encriptada = ContraseñaCompañiumDAO.GenerateSaltedHash(bytes_contraseña, bytes_salt);


            ContraseñaCompañium pass = new ContraseñaCompañium();
            pass.ParteEncriptada = Convert.ToBase64String(parte_encriptada);
            pass.Salt = salt;
            this._context.ContraseñaCompañia.Add(pass);
            this._context.SaveChanges();

            var lista_pass = this._context.ContraseñaCompañia.ToList();
            var obj = lista_pass.Find(u => u.ParteEncriptada == pass.ParteEncriptada);

            clie.IdContraseñaCompañia = obj.IdContraseñaCompañia;

            this._context.Compañia.Add(clie);

            // var lista_cliente_general = base_datos.ClienteGenerals.ToList();
            ClienteGeneral cliente_gen = new ClienteGeneral(clie.IdCompañia, false, true);
            this._context.ClienteGenerals.Add(cliente_gen);

            this._context.SaveChanges();

            return View("CrearCliente");
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
