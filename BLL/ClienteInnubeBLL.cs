﻿using Proyecto_sena.Data;
using Proyecto_sena.Models;
using Proyecto_sena.Models.DAOS;
using System;
using System.Linq;
using System.Text;

namespace Proyecto_sena.BLL
{
    public class ClienteInnubeBLL
    {
        private readonly InnubeDbContext DBContext;
        
        public ClienteInnubeBLL(InnubeDbContext context)
        {
            this.DBContext = context;
        }

        public ClienteInnube ExisteUsuario(string correo, string contraseña)
        {
            var existe = this.DBContext.ClienteInnube.ToList().Exists(c => c.CorreoElectronico == correo);

            if (!existe)
            {
                return null;
            }

            ClienteInnube persona = this.DBContext.ClienteInnube.FirstOrDefault(c => c.CorreoElectronico == correo);

            var salt = persona.Salt;
            byte[] bytes_contraseña = Encoding.UTF8.GetBytes(contraseña);
            byte[] bytes_salt = Encoding.UTF8.GetBytes(salt);

            var contraseña_encriptada = ContraseñaClienteDAO.GenerateSaltedHash(bytes_contraseña, bytes_salt);
            var contraseña_encriptada2 = Convert.ToBase64String(contraseña_encriptada);

            if(contraseña_encriptada2 != persona.ParteEncriptada){
                return null;
            }

            return persona;

        }
    }
}
