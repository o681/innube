﻿using System.ComponentModel.DataAnnotations;

namespace Proyecto_sena.Models
{
    public class Servicio
    {
        [Key]
        public int ServicioID { get; set; }
        public int ClienteInnubeID { get; set; }
        public string Nombre { get; set; }
        public decimal Valor { get; set; }
        public string Descripcion { get; set; }
    }
}
