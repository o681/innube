﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

#nullable disable

namespace Proyecto_sena.Models
{
    public partial class Cotizacion
    {
        public Cotizacion()
        {
            DemandaServicios = new HashSet<DemandaServicio>();
        }
        [Key]
        public uint IdCotizacion { get; set; }
        public uint PrecioTotal { get; set; }
        public string IdClienteGeneral { get; set; }
        public uint Subtotal { get; set; }

         public readonly double  iva=0.16;

        public virtual ClienteGeneral IdClienteGeneralNavigation { get; set; }
        public virtual ICollection<DemandaServicio> DemandaServicios { get; set; }
    }
}
